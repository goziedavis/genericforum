import { Component, OnInit } from '@angular/core';
import { AccountService } from '../_services/account.service';
import { faLock, faIdCard, faUser } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faUser = faUser;
  faLock = faLock;
  faIdCard = faIdCard;
  model: any = {};

  constructor(private accountService: AccountService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  login() {
    this.accountService.login(this.model).subscribe(response => {
      console.log(response)
      this.router.navigateByUrl('/')
    }, error => {
      console.log(error);
      this.toastr.error(error.error)
    })
  }

}
