import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faLock, faIdCard, faUser } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  faUser = faUser;
  faLock = faLock;
  faIdCard = faIdCard;
  model: any = {};

  constructor(private accountService: AccountService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  register() {
    console.log(this.model)
    this.accountService.register(this.model).subscribe(response => {
      console.log(response)
      this.router.navigateByUrl('/')
    }, (error: HttpErrorResponse) => {
      console.log(error.error);
      this.toastr.error(error.error)
    })
  }

  cancel() {
    console.log("Cancelled!")
  }

}
