using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using API.Data;
using API.DTOs;
using API.Entities;
using API.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly DataContext _context;
    private readonly ITokenService _tokenService;
        public AccountController(DataContext context, ITokenService tokenService)
        {
            _context = context;
            _tokenService = tokenService;
        }

        // api/account/register
        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto) {
            if(await usernameExists(registerDto.UserName)) {
                return BadRequest("Username already exists!");
            }

            if(await displayNameExists(registerDto.DisplayName)) {
                return BadRequest("Display name taken by another user!");
            }

            using var hmac = new HMACSHA512();

            var user = new AppUser{
                UserName = registerDto.UserName,
                DisplayName = registerDto.DisplayName,
                PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password)),
                PasswordSalt = hmac.Key
            };

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return new UserDto
            {
                UserName = user.UserName,
                DisplayName = user.DisplayName,
                Token = _tokenService.CreateToken(user)
            };
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login(LoginDto loginDto) {
            var user = await _context.Users
                .SingleOrDefaultAsync(x => (x.UserName == loginDto.UserName) || (x.UserName == loginDto.UserName.ToLower()));

            if(user == null) {
                return Unauthorized("Invalid Username!");
            }

            using var hmac = new HMACSHA512(user.PasswordSalt);
            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

            for(int i = 0; i < computedHash.Length; i++) {
                if(computedHash[i] != user.PasswordHash[i]) {
                    return Unauthorized("Invalid Password!");
                }
            }

            return new UserDto
            {
                UserName = user.UserName,
                DisplayName = user.DisplayName,
                Token = _tokenService.CreateToken(user)
            };
        }

        public async Task<bool> usernameExists(string username) {
            return await _context.Users.AnyAsync(x => (x.UserName == username) || (x.UserName == username.ToLower()));
        }

        public async Task<bool> displayNameExists(string displayName) {
            return await _context.Users.AnyAsync(x => (x.DisplayName == displayName) || (x.DisplayName == displayName.ToLower()));
        }
    }
}