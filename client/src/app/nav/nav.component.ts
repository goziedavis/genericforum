import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../_modals/user';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {}
  loggedIn: boolean
  currentUser$: Observable<User>

  constructor(private accountService: AccountService, private router: Router) { }

  ngOnInit(): void {
    this.currentUser$ = this.accountService.currentUser$
  }

  /*login() {
    this.accountService.login(this.model).subscribe(response => {
      console.log(response)
      this.loggedIn = true
      console.log(this.currentUser$)
    }, error => {
      console.log(error);
    })
  }*/

  logout() {
    this.accountService.logout()
    this.router.navigateByUrl('/')
    this.loggedIn = false
  }

}
