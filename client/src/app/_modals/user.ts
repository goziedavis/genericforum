export interface User {
    username: string
    displayname: string
    token: string
}